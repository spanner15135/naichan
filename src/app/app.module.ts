import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { FixPage } from '../pages/fix/fix';
import { MapPage } from '../pages/map/map';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { AddCarPage } from '../pages/add-car/add-car';
import { TagPage } from '../pages/tag/tag';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ProfilePage } from '../pages/profile/profile';

import { ShopPage } from '../pages/shop/shop';
import { ShopMapPage } from '../pages/shop-map/shop-map';

import { CarInfoPage } from '../pages/car-info/car-info';

import { Fix1Page } from '../pages/fix1/fix1';
import { Fix2Page } from '../pages/fix2/fix2';
import { Fix3Page } from '../pages/fix3/fix3';
import { Fix11Page } from '../pages/fix11/fix11';
import { Fix21Page } from '../pages/fix21/fix21';


@NgModule({
  declarations: [
    MyApp,
    FixPage,
    Fix1Page,
    Fix11Page,
    Fix2Page,
    Fix21Page,
    Fix3Page,
    MapPage,
    HomePage,
    TagPage,
    ShopPage,
    ShopMapPage,
    ProfilePage,
    CarInfoPage,
    TabsPage,
    AddCarPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    FixPage,
    Fix1Page,
    Fix11Page,
    Fix2Page,
    Fix21Page,
    Fix3Page,
    MapPage,
    HomePage,
    TagPage,
    ShopPage,
    ShopMapPage,
    ProfilePage,
    CarInfoPage,
    TabsPage,
    AddCarPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
  ]
})
export class AppModule {}
