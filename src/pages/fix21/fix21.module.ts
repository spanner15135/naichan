import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Fix21Page } from './fix21';

@NgModule({
  declarations: [
    Fix21Page,
  ],
  imports: [
    IonicPageModule.forChild(Fix21Page),
  ],
})
export class Fix21PageModule {}
