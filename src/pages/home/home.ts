import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { TagPage } from '../tag/tag';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  
  tagPage = TagPage;

  constructor(public navCtrl: NavController) {

  }

}
