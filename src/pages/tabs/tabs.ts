import { Component } from '@angular/core';

import { FixPage } from '../fix/fix';
import { MapPage } from '../map/map';
import { HomePage } from '../home/home';
import { ProfilePage } from '../profile/profile';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = FixPage;
  tab3Root = MapPage;
  tab4Root = ProfilePage;

  constructor() {

  }
}
