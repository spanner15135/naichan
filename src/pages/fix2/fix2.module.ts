import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Fix2Page } from './fix2';

@NgModule({
  declarations: [
    Fix2Page,
  ],
  imports: [
    IonicPageModule.forChild(Fix2Page),
  ],
})
export class Fix2PageModule {}
