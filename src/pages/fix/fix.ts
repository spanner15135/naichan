import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Fix1Page } from '../fix1/fix1';
import { Fix3Page } from '../fix3/fix3';
import { Fix2Page } from '../fix2/fix2';

@Component({
  selector: 'page-fix',
  templateUrl: 'fix.html'
})
export class FixPage {

  fix1Page = Fix1Page;
  fix2Page = Fix2Page;
  fix3Page = Fix3Page;

  constructor(public navCtrl: NavController) {

  }

}
