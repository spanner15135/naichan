import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Fix11Page } from './fix11';

@NgModule({
  declarations: [
    Fix11Page,
  ],
  imports: [
    IonicPageModule.forChild(Fix11Page),
  ],
})
export class Fix11PageModule {}
