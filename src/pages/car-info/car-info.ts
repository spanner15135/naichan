import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Car } from '../profile/car'

/**
 * Generated class for the CarInfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-car-info',
  templateUrl: 'car-info.html',
})
export class CarInfoPage {

  currentCar:Car;
  carName:string;
  carManufacturer:string;
  carColor:string;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.currentCar = navParams.get('currentCar');
    this.carName = navParams.get('currentCar').name;
    this.carManufacturer = navParams.get('currentCar').manufacturer;
    this.carColor = navParams.get('currentCar').colorName;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CarInfoPage');
  }

}
