export class Car {

    constructor(
        public id: number,
        public name: string,
        public manufacturer: string,
        public colorName: string,
    ) {}



    colorEncoder():string {
        switch(this.colorName) {
          case "pink": return "#f49ac1";
          case "blue": return "#003ce4";
          case "green": return "#00a651";
        }
      }
}