import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AddCarPage } from '../add-car/add-car';
import { CarInfoPage } from '../car-info/car-info';
import { Car } from '../profile/car';


/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  testText:string = "Test";

  ownerName:string = "นางสาวพัศชนันท์ เจียจิรโชติ";
  ownerNum:string = "098-765-4321";

  cars = [
    new Car(0, "มะงึก", "Toyota","blue"),
    new Car(1, "อุ๋ง", "Honda", "green"),
    new Car(2, "อร", "Mazda", "pink")
  ]

  cBlue = "white";

  carsColor = [];

  addCarPage = AddCarPage;
  carInfoPage = CarInfoPage;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.cars = [
      new Car(0, "มะงึก", "Toyota","blue"),
      new Car(1, "อุ๋ง", "Honda", "green"),
      new Car(2, "อร", "Mazda", "pink")
    ];
    for (let car of this.cars) {
      this.carsColor.push(car.colorEncoder);
    }
  }

  getCarColor (car:Car):string {
    return car.colorEncoder();
  }

  pushParams(car:Car) {
    this.navCtrl.push(this.carInfoPage, { 'currentCar' : car});
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }

}
