import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Fix1Page } from './fix1';

@NgModule({
  declarations: [
    Fix1Page,
  ],
  imports: [
    IonicPageModule.forChild(Fix1Page),
  ],
})
export class Fix1PageModule {}
