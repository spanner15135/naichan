import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Fix11Page } from '../fix11/fix11';
import { Fix21Page } from '../fix21/fix21';

/**
 * Generated class for the Fix1Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-fix1',
  templateUrl: 'fix1.html',
})
export class Fix1Page {

  fix11Page = Fix11Page;
  fix21Page = Fix21Page;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Fix1Page');
  }
  items = [
    {id: 1, name:'ระดับน้ำหล่อเย็นในหม้อน้ำลดต่ำ'},
    {id: 2, name:'หม้อน้ำร้อน (โอเวอร์ฮีท)'},
    {id: 3, name:'ระดับน้ำมันหล่อลื่นต่ำเกินไป'},
    {id: 4, name:'ปั้มน้ำชำรุด'}
  ];

  itemSelected(item: any) {
    if(item.id == 1)
    {      
    // console.log("Selected Item", item);
     this.navCtrl.push(Fix11Page);
    }
    else if(item.id == 2)
    {
      this.navCtrl.push(Fix21Page);
    }
    }

}
