import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Fix3Page } from './fix3';

@NgModule({
  declarations: [
    Fix3Page,
  ],
  imports: [
    IonicPageModule.forChild(Fix3Page),
  ],
})
export class Fix3PageModule {}
