import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

/**
 * Generated class for the SubfixPage tabs.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-subfix',
  templateUrl: 'subfix.html'
})
export class SubfixPage {

  fix1PageRoot = 'Fix1Page'
  fix2PageRoot = 'Fix2Page'
  fix3PageRoot = 'Fix3Page'


  constructor(public navCtrl: NavController) {}

}
