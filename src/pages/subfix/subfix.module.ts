import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SubfixPage } from './subfix';

@NgModule({
  declarations: [
    SubfixPage,
  ],
  imports: [
    IonicPageModule.forChild(SubfixPage),
  ]
})
export class SubfixPageModule {}
