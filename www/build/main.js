webpackJsonp([12],{

/***/ 100:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Fix1Page; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__fix11_fix11__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__fix21_fix21__ = __webpack_require__(206);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the Fix1Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var Fix1Page = /** @class */ (function () {
    function Fix1Page(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.fix11Page = __WEBPACK_IMPORTED_MODULE_2__fix11_fix11__["a" /* Fix11Page */];
        this.fix21Page = __WEBPACK_IMPORTED_MODULE_3__fix21_fix21__["a" /* Fix21Page */];
        this.items = [
            { id: 1, name: 'ระดับน้ำหล่อเย็นในหม้อน้ำลดต่ำ' },
            { id: 2, name: 'หม้อน้ำร้อน (โอเวอร์ฮีท)' },
            { id: 3, name: 'ระดับน้ำมันหล่อลื่นต่ำเกินไป' },
            { id: 4, name: 'ปั้มน้ำชำรุด' }
        ];
    }
    Fix1Page.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad Fix1Page');
    };
    Fix1Page.prototype.itemSelected = function (item) {
        if (item.id == 1) {
            // console.log("Selected Item", item);
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__fix11_fix11__["a" /* Fix11Page */]);
        }
        else if (item.id == 2) {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__fix21_fix21__["a" /* Fix21Page */]);
        }
    };
    Fix1Page = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-fix1',template:/*ion-inline-start:"C:\Users\User\Documents\naichan\src\pages\fix1\fix1.html"*/'<!--\n  Generated template for the Fix1Page page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n    <style>\n        #circle {\n          width: 50px;\n          height: 50px;\n          -webkit-border-radius: 25px;\n          -moz-border-radius: 25px;\n          border-radius: 25px;\n          background: rgb(255, 187, 28);\n        }\n      </style>\n\n  <ion-navbar>\n    <ion-title><font class="font1">เครื่องยนต์รถ</font></ion-title>\n  </ion-navbar>\n\n</ion-header>\n<link href="https://fonts.googleapis.com/css?family=Kanit" rel="stylesheet">\n\n<style>\n.font1 {\n  font-family: \'Kanit\', sans-serif;\n}\n.button {\n  color:gray;\n  text-align: middle;\n  font-family: \'Kanit\', sans-serif;\n}\n</style>\n\n<ion-content padding>\n    <ion-list class="font1">\n        <button ion-item *ngFor="let item of items" (click)="itemSelected(item) ">\n          {{ item.name }}\n        </button>  \n      </ion-list>\n</ion-content>\n'/*ion-inline-end:"C:\Users\User\Documents\naichan\src\pages\fix1\fix1.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], Fix1Page);
    return Fix1Page;
}());

//# sourceMappingURL=fix1.js.map

/***/ }),

/***/ 101:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Fix11Page; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the Fix11Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var Fix11Page = /** @class */ (function () {
    function Fix11Page(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    Fix11Page.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad Fix11Page');
    };
    Fix11Page = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-fix11',template:/*ion-inline-start:"C:\Users\User\Documents\naichan\src\pages\fix11\fix11.html"*/'<!--\n  Generated template for the Fix11Page page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <style>\n    #circle {\n      width: 50px;\n      height: 50px;\n      -webkit-border-radius: 25px;\n      -moz-border-radius: 25px;\n      border-radius: 25px;\n      background: rgb(255, 187, 28);\n    }\n  </style>\n  <ion-navbar>\n    <ion-title>\n      <font class="font1">ระดับน้ำหล่อเย็นในหม้อน้ำลดต่ำ</font>\n    </ion-title>\n  </ion-navbar>\n\n</ion-header>\n<style>\n  .font1 {\n    font-family: \'Kanit\', sans-serif;\n  }\n</style>\n\n<ion-content padding>\n  <ion-card-content>\n    <ion-card-title>\n      <span class="profile-name">{{ownerName}}</span>\n    </ion-card-title>\n    <p>\n      <span class="profile-tel">โทร: {{ownerNum}}</span>\n    </p>\n  </ion-card-content>\n</ion-content>'/*ion-inline-end:"C:\Users\User\Documents\naichan\src\pages\fix11\fix11.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], Fix11Page);
    return Fix11Page;
}());

//# sourceMappingURL=fix11.js.map

/***/ }),

/***/ 102:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Fix2Page; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the Fix2Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var Fix2Page = /** @class */ (function () {
    function Fix2Page(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.items = [
            'Pokémon Yellow',
            'Super Metroid',
            'Mega Man X',
            'The Legend of Zelda',
            'Pac-Man',
            'Super Mario World',
            'Street Fighter II',
            'Half Life',
            'Final Fantasy VII',
            'Star Fox',
            'Tetris',
            'Donkey Kong III',
            'GoldenEye 007',
            'Doom',
            'Fallout',
            'GTA',
            'Halo'
        ];
    }
    Fix2Page.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad Fix2Page');
    };
    Fix2Page.prototype.itemSelected = function (item) {
        console.log("Selected Item", item);
    };
    Fix2Page = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-fix2',template:/*ion-inline-start:"C:\Users\User\Documents\naichan\src\pages\fix2\fix2.html"*/'<!--\n  Generated template for the Fix2Page page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n    <style>\n        #circle {\n          width: 50px;\n          height: 50px;\n          -webkit-border-radius: 25px;\n          -moz-border-radius: 25px;\n          border-radius: 25px;\n          background: rgb(255, 187, 28);\n        }\n      </style>\n  <ion-navbar>\n    <ion-title><font class="font1">ช่วงล่าง</font></ion-title>\n  </ion-navbar>\n\n</ion-header>\n<link href="https://fonts.googleapis.com/css?family=Kanit" rel="stylesheet">\n\n<style>\n.font1 {\n  font-family: \'Kanit\', sans-serif;\n}\n.button {\n  color:gray;\n  text-align: middle;\n  font-family: \'Kanit\', sans-serif;\n}\n</style>\n\n<ion-content padding>\n    <ion-list>\n        <button ion-item *ngFor="let item of items" (click)="itemSelected(item)">\n          {{ item }}\n        </button>  \n      </ion-list>\n</ion-content>\n'/*ion-inline-end:"C:\Users\User\Documents\naichan\src\pages\fix2\fix2.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], Fix2Page);
    return Fix2Page;
}());

//# sourceMappingURL=fix2.js.map

/***/ }),

/***/ 103:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddCarPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the AddCarPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AddCarPage = /** @class */ (function () {
    function AddCarPage(navCtrl, navParams, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
    }
    AddCarPage.prototype.doAlert = function () {
        var alert = this.alertCtrl.create({
            title: this.manufacturer,
            subTitle: 'Ya',
            buttons: ['FUCK']
        });
        alert.present();
    };
    AddCarPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AddCarPage');
    };
    AddCarPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-add-car',template:/*ion-inline-start:"C:\Users\User\Documents\naichan\src\pages\add-car\add-car.html"*/'<!--\n\n  Generated template for the AddCarPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>เพิ่มรถของคุณ</ion-title>\n\n\n\n    <ion-buttons end>\n\n      <button ion-button clear small icon-start (click)="doAlert()" >\n\n        <ion-icon style="font-size:30px" color="primary" name="checkmark"></ion-icon>\n\n        <span class="right-header-button" color="primary">เสร็จ</span>\n\n      </button>\n\n    </ion-buttons>\n\n\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n  <ion-list>\n\n\n\n    <ion-list-header>ข้อมูลรถ</ion-list-header>\n\n\n\n    <ion-item>\n\n      <ion-input type="text" placeholder="ชื่อรถของคุณ"></ion-input>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-label>บริษัทผู้ผลิต</ion-label>\n\n      <ion-select [(ngModel)]="manufacturer">\n\n        <ion-option value="toyota">Toyota</ion-option>\n\n        <ion-option value="honda">Honda</ion-option>\n\n        <ion-option value="mazda">Mazda</ion-option>\n\n        <ion-option value="nissan">Nissan</ion-option>\n\n        <ion-option value="ford">Ford</ion-option>\n\n      </ion-select>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-label>สีรถ</ion-label>\n\n      <ion-select [(ngModel)]="color">\n\n        <ion-option value="black">ดำ</ion-option>\n\n        <ion-option value="white">ขาว</ion-option>\n\n        <ion-option value="blonde">บลอน</ion-option>\n\n        <ion-option value="navy">กรมท่า</ion-option>\n\n        <ion-option value="gold">ทอง</ion-option>\n\n      </ion-select>\n\n    </ion-item>\n\n\n\n  </ion-list>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\User\Documents\naichan\src\pages\add-car\add-car.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], AddCarPage);
    return AddCarPage;
}());

//# sourceMappingURL=add-car.js.map

/***/ }),

/***/ 104:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Fix3Page; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the Fix3Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var Fix3Page = /** @class */ (function () {
    function Fix3Page(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.items = [
            'Pokémon Yellow',
            'Super Metroid',
            'Mega Man X',
            'The Legend of Zelda',
            'Pac-Man',
            'Super Mario World',
            'Street Fighter II',
            'Half Life',
            'Final Fantasy VII',
            'Star Fox',
            'Tetris',
            'Donkey Kong III',
            'GoldenEye 007',
            'Doom',
            'Fallout',
            'GTA',
            'Halo'
        ];
    }
    Fix3Page.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad Fix3Page');
    };
    Fix3Page.prototype.itemSelected = function (item) {
        console.log("Selected Item", item);
    };
    Fix3Page = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-fix3',template:/*ion-inline-start:"C:\Users\User\Documents\naichan\src\pages\fix3\fix3.html"*/'<!--\n  Generated template for the Fix3Page page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n    <style>\n        #circle {\n          width: 50px;\n          height: 50px;\n          -webkit-border-radius: 25px;\n          -moz-border-radius: 25px;\n          border-radius: 25px;\n          background: rgb(255, 187, 28);\n        }\n      </style>\n  <ion-navbar>\n    <ion-title><font class="font1">ล้อ</font></ion-title>\n  </ion-navbar>\n\n</ion-header>\n<link href="https://fonts.googleapis.com/css?family=Kanit" rel="stylesheet">\n\n<style>\n.font1 {\n  font-family: \'Kanit\', sans-serif;\n}\n.button {\n  color:gray;\n  text-align: middle;\n  font-family: \'Kanit\', sans-serif;\n}\n</style>\n\n<ion-content padding>\n    <ion-list>\n        <button ion-item *ngFor="let item of items" (click)="itemSelected(item)">\n          {{ item }}\n        </button>  \n      </ion-list>\n</ion-content>\n'/*ion-inline-end:"C:\Users\User\Documents\naichan\src\pages\fix3\fix3.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], Fix3Page);
    return Fix3Page;
}());

//# sourceMappingURL=fix3.js.map

/***/ }),

/***/ 105:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__add_car_add_car__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__car_info_car_info__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__profile_car__ = __webpack_require__(257);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ProfilePage = /** @class */ (function () {
    function ProfilePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.testText = "Test";
        this.ownerName = "นางสาวพัศชนันท์ เจียจิรโชติ";
        this.ownerNum = "098-765-4321";
        this.cars = [
            new __WEBPACK_IMPORTED_MODULE_4__profile_car__["a" /* Car */](0, "มะงึก", "Toyota", "blue"),
            new __WEBPACK_IMPORTED_MODULE_4__profile_car__["a" /* Car */](1, "อุ๋ง", "Honda", "green"),
            new __WEBPACK_IMPORTED_MODULE_4__profile_car__["a" /* Car */](2, "อร", "Mazda", "pink")
        ];
        this.cBlue = "white";
        this.carsColor = [];
        this.addCarPage = __WEBPACK_IMPORTED_MODULE_2__add_car_add_car__["a" /* AddCarPage */];
        this.carInfoPage = __WEBPACK_IMPORTED_MODULE_3__car_info_car_info__["a" /* CarInfoPage */];
        this.cars = [
            new __WEBPACK_IMPORTED_MODULE_4__profile_car__["a" /* Car */](0, "มะงึก", "Toyota", "blue"),
            new __WEBPACK_IMPORTED_MODULE_4__profile_car__["a" /* Car */](1, "อุ๋ง", "Honda", "green"),
            new __WEBPACK_IMPORTED_MODULE_4__profile_car__["a" /* Car */](2, "อร", "Mazda", "pink")
        ];
        for (var _i = 0, _a = this.cars; _i < _a.length; _i++) {
            var car = _a[_i];
            this.carsColor.push(car.colorEncoder);
        }
    }
    ProfilePage.prototype.getCarColor = function (car) {
        return car.colorEncoder();
    };
    ProfilePage.prototype.pushParams = function (car) {
        this.navCtrl.push(this.carInfoPage, { 'currentCar': car });
    };
    ProfilePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ProfilePage');
    };
    ProfilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-profile',template:/*ion-inline-start:"C:\Users\User\Documents\naichan\src\pages\profile\profile.html"*/'<!--\n\n  Generated template for the ProfilePage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-content padding scroll="false">\n\n  <div class="div-profile-pic">\n\n    <img class="profile-pic" src="img/profile.png"/>\n\n\n\n  <ion-card class="div-profile-info">\n\n    <div style="height:60px">\n\n    <ion-card-content>\n\n      <ion-card-title>\n\n        <span class="profile-name">{{ownerName}}</span>\n\n      </ion-card-title>\n\n        <p>\n\n          <span class="profile-tel">โทร: {{ownerNum}}</span>\n\n        </p>\n\n      </ion-card-content>\n\n    </div>\n\n    <ion-row no-padding>\n\n      <ion-col text-right>\n\n        <button ion-button clear small icon-start style="color:grey;font-family: \'Kanit\', sans-serif;">\n\n          <ion-icon name=\'brush\'></ion-icon><span style="color:grey;">แก้ไขโปรไฟล์</span>\n\n        </button>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-card>\n\n  </div>\n\n\n\n  <div class="div-car-list">\n\n    <ion-slides loop="false" pager>\n\n      <ion-slide *ngFor="let car of cars">\n\n        <ion-card>\n\n          <div class="div-car-color"><img id="car-color" src="img/car-color/{{car.colorName}}"/></div>\n\n          <ion-card-content>\n\n            <ion-card-title>\n\n              {{car.name}}\n\n            </ion-card-title>\n\n            <p>\n\n              {{car.colorName}}\n\n              {{car.manufacturer}}\n\n            </p>\n\n         </ion-card-content>\n\n        \n\n          <ion-row no-padding>\n\n            <ion-col text-right>\n\n              <button ion-button clear small\n\n              style="color:grey;font-family: \'Kanit\', sans-serif;"\n\n              icon-start (click)="pushParams(car)">\n\n                <ion-icon name=\'ios-eye\'></ion-icon>\n\n                ดูรายละเอียด\n\n              </button>\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-card>\n\n      </ion-slide>\n\n    </ion-slides>\n\n  </div>\n\n\n\n  <button class="add-button" color="light" ion-button block outline [navPush]="addCarPage">\n\n    <div style="width:0px;"><ion-icon name="ios-add"></ion-icon></div>\n\n    <span class="add-car-text">เพิ่มรถของคุณ</span>\n\n  </button>\n\n</ion-content>\n\n<link href="https://fonts.googleapis.com/css?family=Kanit" rel="stylesheet">\n\n\n\n<style>\n\n.div-car-list {\n\n  font-family: \'Kanit\', sans-serif;\n\n}\n\n</style>\n\n'/*ion-inline-end:"C:\Users\User\Documents\naichan\src\pages\profile\profile.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], ProfilePage);
    return ProfilePage;
}());

//# sourceMappingURL=profile.js.map

/***/ }),

/***/ 106:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ShopPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the ShopPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ShopPage = /** @class */ (function () {
    function ShopPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ShopPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ShopPage');
    };
    ShopPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-shop',template:/*ion-inline-start:"C:\Users\User\Documents\naichan\src\pages\shop\shop.html"*/'<!--\n\n  Generated template for the ShopPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>shop</ion-title>\n\n  </ion-navbar>\n\n\n\n  <style>\n\n      .font1 {\n\n        font-family: \'Kanit\', sans-serif;\n\n      }\n\n      </style>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n  <ion-row>\n\n      <button class="font1" ion-button full color="light">อู่ชัชชวาลย์</button>\n\n  </ion-row>\n\n  <ion-row>\n\n      <button class="font1" ion-button full color="light">ช่างยนต์เฉลิมชัย</button>\n\n  </ion-row>\n\n  <ion-row>\n\n      <button class="font1" ion-button full color="light">ต๊ะวันรถแต่ง</button>\n\n  </ion-row>\n\n  <ion-row>\n\n      <button class="font1" ion-button full color="light">ฟอร์ดเครื่องยนต์</button>\n\n  </ion-row>\n\n  <ion-row>\n\n      <button class="font1" ion-button full color="light">ฟิล์มรถสมชาย</button>\n\n  </ion-row>\n\n</ion-content>'/*ion-inline-end:"C:\Users\User\Documents\naichan\src\pages\shop\shop.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], ShopPage);
    return ShopPage;
}());

//# sourceMappingURL=shop.js.map

/***/ }),

/***/ 107:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TagPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shop_shop__ = __webpack_require__(106);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the TagPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TagPage = /** @class */ (function () {
    function TagPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.shopPage = __WEBPACK_IMPORTED_MODULE_2__shop_shop__["a" /* ShopPage */];
    }
    TagPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TagPage');
    };
    TagPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-tag',template:/*ion-inline-start:"C:\Users\User\Documents\naichan\src\pages\tag\tag.html"*/'<!--\n\n  Generated template for the TagPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n  </ion-navbar>\n\n  <style>\n\n    .button1{\n\n      display: block;\n\n      margin-left: auto;\n\n      margin-right: auto;\n\n    }\n\n\n\n  </style>\n\n\n\n</ion-header>\n\n\n\n<style>\n\n    .font {\n\n      font-family: \'Kanit\', sans-serif;\n\n    }\n\n    .button1 {\n\n      \n\n      text-align: middle;\n\n      font-family: \'Kanit\', sans-serif;\n\n    }\n\n    </style>\n\n\n\n\n\n<ion-content padding>\n\n    <ion-grid class="font">\n\n        <ion-row item-width="100%">\n\n            <h1>ซ่อม</h1>\n\n        </ion-row>\n\n        <ion-row item-width="100%">\n\n            <ion-list>\n\n\n\n                <ion-item>\n\n                  <ion-label>ช่วงล่าง</ion-label>\n\n                  <ion-checkbox [(ngModel)]="lowerbody"></ion-checkbox>\n\n                </ion-item>\n\n              \n\n                <ion-item>\n\n                  <ion-label>ตัวรถ</ion-label>\n\n                  <ion-checkbox [(ngModel)]="body" ></ion-checkbox>\n\n                </ion-item>\n\n              \n\n              </ion-list>\n\n        </ion-row>\n\n        <ion-row>\n\n            <h1>บำรุง</h1>\n\n        </ion-row>\n\n        <ion-row>\n\n            <ion-list>\n\n\n\n                <ion-item>\n\n                  <ion-label>เปลี่ยนน้ำมันเครื่อง</ion-label>\n\n                  <ion-checkbox [(ngModel)]="oil"></ion-checkbox>\n\n                </ion-item>\n\n              \n\n                <ion-item>\n\n                  <ion-label>แบตเตอร์รี่</ion-label>\n\n                  <ion-checkbox [(ngModel)]="batery" ></ion-checkbox>\n\n                </ion-item>\n\n              \n\n              </ion-list>\n\n        </ion-row>\n\n        \n\n        <ion-row>\n\n            <h1>ตกแต่ง</h1>\n\n        </ion-row>\n\n        <ion-row>\n\n            <ion-list>\n\n\n\n                <ion-item>\n\n                  <ion-label>เปลี่ยนยาง/แม็กซ์</ion-label>\n\n                  <ion-checkbox [(ngModel)]="tire"></ion-checkbox>\n\n                </ion-item>\n\n              \n\n                <ion-item>\n\n                  <ion-label>เปลี่ยนล้อ</ion-label>\n\n                  <ion-checkbox [(ngModel)]="wheel" ></ion-checkbox>\n\n                </ion-item>\n\n\n\n                <ion-item>\n\n                    <ion-label>แต่งรถ</ion-label>\n\n                    <ion-checkbox [(ngModel)]="deco" ></ion-checkbox>\n\n                  </ion-item>\n\n\n\n                  <ion-item>\n\n                      <ion-label>ฟิล์มรถยนต์</ion-label>\n\n                      <ion-checkbox [(ngModel)]="film" ></ion-checkbox>\n\n                    </ion-item>\n\n\n\n                    <ion-item>\n\n                        <ion-label>โหลดรถ/ยกสูง</ion-label>\n\n                        <ion-checkbox [(ngModel)]="load" ></ion-checkbox>\n\n                      </ion-item>\n\n              \n\n              </ion-list>\n\n        </ion-row>\n\n      </ion-grid>\n\n      \n\n      <button class="button1" ion-button round [navPush]="shopPage">ค้นหา</button>\n\n      \n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\User\Documents\naichan\src\pages\tag\tag.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], TagPage);
    return TagPage;
}());

//# sourceMappingURL=tag.js.map

/***/ }),

/***/ 117:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 117;

/***/ }),

/***/ 158:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/add-car/add-car.module": [
		287,
		11
	],
	"../pages/car-info/car-info.module": [
		283,
		10
	],
	"../pages/fix1/fix1.module": [
		284,
		8
	],
	"../pages/fix11/fix11.module": [
		285,
		9
	],
	"../pages/fix2/fix2.module": [
		286,
		6
	],
	"../pages/fix21/fix21.module": [
		288,
		7
	],
	"../pages/fix3/fix3.module": [
		289,
		5
	],
	"../pages/profile/profile.module": [
		290,
		4
	],
	"../pages/shop-map/shop-map.module": [
		291,
		3
	],
	"../pages/shop/shop.module": [
		292,
		2
	],
	"../pages/subfix/subfix.module": [
		293,
		0
	],
	"../pages/tag/tag.module": [
		294,
		1
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 158;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 202:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__fix_fix__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__map_map__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__home_home__ = __webpack_require__(205);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__profile_profile__ = __webpack_require__(105);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var TabsPage = /** @class */ (function () {
    function TabsPage() {
        this.tab1Root = __WEBPACK_IMPORTED_MODULE_3__home_home__["a" /* HomePage */];
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_1__fix_fix__["a" /* FixPage */];
        this.tab3Root = __WEBPACK_IMPORTED_MODULE_2__map_map__["a" /* MapPage */];
        this.tab4Root = __WEBPACK_IMPORTED_MODULE_4__profile_profile__["a" /* ProfilePage */];
    }
    TabsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\Users\User\Documents\naichan\src\pages\tabs\tabs.html"*/'<ion-tabs>\n\n  <ion-tab [root]="tab1Root" tabIcon="ios-home"></ion-tab>\n\n  <ion-tab [root]="tab2Root" tabIcon="ios-construct"></ion-tab>\n\n  <ion-tab [root]="tab3Root" tabIcon="ios-navigate"></ion-tab>\n\n  <ion-tab [root]="tab4Root" tabIcon="ios-contact"></ion-tab>\n\n</ion-tabs>\n\n'/*ion-inline-end:"C:\Users\User\Documents\naichan\src\pages\tabs\tabs.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], TabsPage);
    return TabsPage;
}());

//# sourceMappingURL=tabs.js.map

/***/ }),

/***/ 203:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FixPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__fix1_fix1__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__fix3_fix3__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__fix2_fix2__ = __webpack_require__(102);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var FixPage = /** @class */ (function () {
    function FixPage(navCtrl) {
        this.navCtrl = navCtrl;
        this.fix1Page = __WEBPACK_IMPORTED_MODULE_2__fix1_fix1__["a" /* Fix1Page */];
        this.fix2Page = __WEBPACK_IMPORTED_MODULE_4__fix2_fix2__["a" /* Fix2Page */];
        this.fix3Page = __WEBPACK_IMPORTED_MODULE_3__fix3_fix3__["a" /* Fix3Page */];
    }
    FixPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-fix',template:/*ion-inline-start:"C:\Users\User\Documents\naichan\src\pages\fix\fix.html"*/'<ion-header>\n\n  <style>\n\n    #circle {\n\n      width: 50px;\n\n      height: 50px;\n\n      -webkit-border-radius: 25px;\n\n      -moz-border-radius: 25px;\n\n      border-radius: 25px;\n\n      background: rgb(255, 187, 28);\n\n    }\n\n  </style>\n\n  <ion-navbar>\n\n    <ion-title>\n\n      <font class="font1">ซ่อมบำรุง</font>\n\n    </ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n<link href="https://fonts.googleapis.com/css?family=Kanit" rel="stylesheet">\n\n\n\n<style>\n\n  .font1 {\n\n    font-family: \'Kanit\', sans-serif;\n\n  }\n\n\n\n  .button {\n\n    color: gray;\n\n    text-align: middle;\n\n    font-family: \'Kanit\', sans-serif;\n\n  }\n\n\n\n  .vertical-align-content {\n\n    display: flex !important;\n\n    align-content: center !important;\n\n    align-items: center !important;\n\n  }\n\n\n\n  .Absolute-Center {\n\n    width: 100%;\n\n    height: 100%;\n\n    /* //overflow: auto; */\n\n    margin: auto;\n\n    position: absolute;\n\n    top: 0;\n\n    left: 0;\n\n    bottom: 0;\n\n    right: 0;\n\n  }\n\n</style>\n\n\n\n<ion-content padding text-center style="height: 100%;">\n\n\n\n  <div class="box boxOut">\n\n    <button ion-button round color="light" [navPush]="fix1Page">\n\n      <font class="button">เครื่องยนต์รถ</font>\n\n    </button>\n\n    <div id="img_container">\n\n      <img class="img-car" style="width: 100%; height: 30%;" src="img/651.png" />\n\n    </div>\n\n    <button ion-button round color="light" [navPush]="fix2Page">\n\n      <font class="button">ช่วงล่าง</font>\n\n    </button>\n\n    <button ion-button round color="light" [navPush]="fix3Page">\n\n      <font class="button">ล้อ</font>\n\n    </button>\n\n  </div>\n\n\n\n\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\User\Documents\naichan\src\pages\fix\fix.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */]])
    ], FixPage);
    return FixPage;
}());

//# sourceMappingURL=fix.js.map

/***/ }),

/***/ 204:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MapPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_google_maps__ = __webpack_require__(296);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var MapPage = /** @class */ (function () {
    function MapPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    MapPage.prototype.ionViewDidload = function () {
        this.loadmap();
    };
    MapPage.prototype.loadmap = function () {
        var mapOptions = {
            camera: {
                target: {
                    lat: 43.0741904,
                    lng: -89.3809802
                },
                zoom: 18,
                tilt: 30
            }
        };
        this.map = __WEBPACK_IMPORTED_MODULE_2__ionic_native_google_maps__["a" /* GoogleMaps */].create('map_canvas', mapOptions);
    };
    MapPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-map',template:/*ion-inline-start:"C:\Users\User\Documents\naichan\src\pages\map\map.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>\n\n       <font class="font1">ร้านใกล้เคียง</font> \n\n    </ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n<link href="https://fonts.googleapis.com/css?family=Kanit" rel="stylesheet">s\n\n<style>\n\n  .font1 {\n\n    font-family: \'Kanit\', sans-serif;\n\n  }\n\n  .button {\n\n    color:gray;\n\n    text-align: left;\n\n\n\n  }\n\n  </style>\n\n\n\n<ion-content padding>\n\n    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d30610.320563659498!2d102.80796435932619!3d16.460843474473155!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31228af57ea7de97%3A0xdd7b17e1de7568cb!2sSrinagarind+Hospital!5e0!3m2!1sen!2sth!4v1532071989961" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\User\Documents\naichan\src\pages\map\map.html"*/
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]) === "function" && _b || Object])
    ], MapPage);
    return MapPage;
    var _a, _b;
}());

//# sourceMappingURL=map.js.map

/***/ }),

/***/ 205:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__tag_tag__ = __webpack_require__(107);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var HomePage = /** @class */ (function () {
    function HomePage(navCtrl) {
        this.navCtrl = navCtrl;
        this.tagPage = __WEBPACK_IMPORTED_MODULE_2__tag_tag__["a" /* TagPage */];
    }
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"C:\Users\User\Documents\naichan\src\pages\home\home.html"*/'<ion-header>\n\n  <!-- <ion-navbar>\n\n    <ion-title>Home</ion-title>\n\n  </ion-navbar> -->\n\n\n\n</ion-header>\n\n<link href="https://fonts.googleapis.com/css?family=Kanit" rel="stylesheet">\n\n\n\n<style>\n\n  .font1 {\n\n    font-family: \'Kanit\', sans-serif;\n\n  }\n\n\n\n  .search {\n\n    color: gray;\n\n    /* margin-right: 30px; */\n\n  }\n\n\n\n  .vertical-align-content {\n\n    display: flex !important;\n\n    align-content: center !important;\n\n    align-items: center !important;\n\n  }\n\n\n\n  .Absolute-Center {\n\n    width: 100%;\n\n    height: 100%;\n\n    /* //overflow: auto; */\n\n    margin: auto;\n\n    position: absolute;\n\n    top: 0;\n\n    left: 0;\n\n    bottom: 0;\n\n    right: 0;\n\n  }\n\n</style>\n\n\n\n\n\n\n\n<ion-content padding text-center>\n\n\n\n\n\n  <div id="img_container">\n\n\n\n    <!-- <img class="img-car" style="width: 50%; height: 50%;" src="img/logo2.png" /> -->\n\n    <h2 style="color: rgb(255, 255, 255)" class="font1">\n\n      <font size="40">นายช่าง</font>\n\n    </h2>\n\n    <button ion-button round color="light" [navPush]="tagPage">\n\n      <font class="search">ค้นหาร้านซ่อมรถใกล้ตัว</font>\n\n    </button>\n\n\n\n\n\n    <p>\n\n      <font size="5" class="font1" style="color: white">รถของคุณมีปัญหาอะไร?</font>\n\n    </p>\n\n  </div>'/*ion-inline-end:"C:\Users\User\Documents\naichan\src\pages\home\home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 206:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Fix21Page; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the Fix21Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var Fix21Page = /** @class */ (function () {
    function Fix21Page(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    Fix21Page.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad Fix21Page');
    };
    Fix21Page = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-fix21',template:/*ion-inline-start:"C:\Users\User\Documents\naichan\src\pages\fix21\fix21.html"*/'<!--\n  Generated template for the Fix21Page page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n    <style>\n        #circle {\n          width: 50px;\n          height: 50px;\n          -webkit-border-radius: 25px;\n          -moz-border-radius: 25px;\n          border-radius: 25px;\n          background: rgb(255, 187, 28);\n        }\n      </style>\n  <ion-navbar>\n    <ion-title><font class="font1">หม้อน้ำร้อน (โอเวอร์ฮีท)</font></ion-title>\n  </ion-navbar>\n\n</ion-header>\n<style>\n    .font1 {\n      font-family: \'Kanit\', sans-serif;\n    }\n    </style>\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"C:\Users\User\Documents\naichan\src\pages\fix21\fix21.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], Fix21Page);
    return Fix21Page;
}());

//# sourceMappingURL=fix21.js.map

/***/ }),

/***/ 207:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ShopMapPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the ShopMapPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ShopMapPage = /** @class */ (function () {
    function ShopMapPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ShopMapPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ShopMapPage');
    };
    ShopMapPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-shop-map',template:/*ion-inline-start:"C:\Users\User\Documents\naichan\src\pages\shop-map\shop-map.html"*/'<!--\n\n  Generated template for the ShopMapPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>shopMap</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\User\Documents\naichan\src\pages\shop-map\shop-map.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], ShopMapPage);
    return ShopMapPage;
}());

//# sourceMappingURL=shop-map.js.map

/***/ }),

/***/ 208:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(209);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(231);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 231:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(275);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_fix_fix__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_map_map__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_home_home__ = __webpack_require__(205);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_tabs_tabs__ = __webpack_require__(202);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_add_car_add_car__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_tag_tag__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_status_bar__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_splash_screen__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_profile_profile__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_shop_shop__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_shop_map_shop_map__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_car_info_car_info__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_fix1_fix1__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_fix2_fix2__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_fix3_fix3__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_fix11_fix11__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_fix21_fix21__ = __webpack_require__(206);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_4__pages_fix_fix__["a" /* FixPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_fix1_fix1__["a" /* Fix1Page */],
                __WEBPACK_IMPORTED_MODULE_19__pages_fix11_fix11__["a" /* Fix11Page */],
                __WEBPACK_IMPORTED_MODULE_17__pages_fix2_fix2__["a" /* Fix2Page */],
                __WEBPACK_IMPORTED_MODULE_20__pages_fix21_fix21__["a" /* Fix21Page */],
                __WEBPACK_IMPORTED_MODULE_18__pages_fix3_fix3__["a" /* Fix3Page */],
                __WEBPACK_IMPORTED_MODULE_5__pages_map_map__["a" /* MapPage */],
                __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_tag_tag__["a" /* TagPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_shop_shop__["a" /* ShopPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_shop_map_shop_map__["a" /* ShopMapPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_profile_profile__["a" /* ProfilePage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_car_info_car_info__["a" /* CarInfoPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_add_car_add_car__["a" /* AddCarPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/car-info/car-info.module#CarInfoPageModule', name: 'CarInfoPage', segment: 'car-info', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/fix1/fix1.module#Fix1PageModule', name: 'Fix1Page', segment: 'fix1', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/fix11/fix11.module#Fix11PageModule', name: 'Fix11Page', segment: 'fix11', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/fix2/fix2.module#Fix2PageModule', name: 'Fix2Page', segment: 'fix2', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/add-car/add-car.module#AddCarPageModule', name: 'AddCarPage', segment: 'add-car', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/fix21/fix21.module#Fix21PageModule', name: 'Fix21Page', segment: 'fix21', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/fix3/fix3.module#Fix3PageModule', name: 'Fix3Page', segment: 'fix3', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/profile/profile.module#ProfilePageModule', name: 'ProfilePage', segment: 'profile', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/shop-map/shop-map.module#ShopMapPageModule', name: 'ShopMapPage', segment: 'shop-map', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/shop/shop.module#ShopPageModule', name: 'ShopPage', segment: 'shop', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/subfix/subfix.module#SubfixPageModule', name: 'SubfixPage', segment: 'subfix', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/tag/tag.module#TagPageModule', name: 'TagPage', segment: 'tag', priority: 'low', defaultHistory: [] }
                    ]
                })
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_4__pages_fix_fix__["a" /* FixPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_fix1_fix1__["a" /* Fix1Page */],
                __WEBPACK_IMPORTED_MODULE_19__pages_fix11_fix11__["a" /* Fix11Page */],
                __WEBPACK_IMPORTED_MODULE_17__pages_fix2_fix2__["a" /* Fix2Page */],
                __WEBPACK_IMPORTED_MODULE_20__pages_fix21_fix21__["a" /* Fix21Page */],
                __WEBPACK_IMPORTED_MODULE_18__pages_fix3_fix3__["a" /* Fix3Page */],
                __WEBPACK_IMPORTED_MODULE_5__pages_map_map__["a" /* MapPage */],
                __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_tag_tag__["a" /* TagPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_shop_shop__["a" /* ShopPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_shop_map_shop_map__["a" /* ShopMapPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_profile_profile__["a" /* ProfilePage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_car_info_car_info__["a" /* CarInfoPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_add_car_add_car__["a" /* AddCarPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_10__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_11__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicErrorHandler */] },
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 257:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Car; });
var Car = /** @class */ (function () {
    function Car(id, name, manufacturer, colorName) {
        this.id = id;
        this.name = name;
        this.manufacturer = manufacturer;
        this.colorName = colorName;
    }
    Car.prototype.colorEncoder = function () {
        switch (this.colorName) {
            case "pink": return "#f49ac1";
            case "blue": return "#003ce4";
            case "green": return "#00a651";
        }
    };
    return Car;
}());

//# sourceMappingURL=car.js.map

/***/ }),

/***/ 275:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_tabs_tabs__ = __webpack_require__(202);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_tabs_tabs__["a" /* TabsPage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\Users\User\Documents\naichan\src\app\app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n\n'/*ion-inline-end:"C:\Users\User\Documents\naichan\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 99:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CarInfoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the CarInfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CarInfoPage = /** @class */ (function () {
    function CarInfoPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.currentCar = navParams.get('currentCar');
        this.carName = navParams.get('currentCar').name;
        this.carManufacturer = navParams.get('currentCar').manufacturer;
        this.carColor = navParams.get('currentCar').colorName;
    }
    CarInfoPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CarInfoPage');
    };
    CarInfoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-car-info',template:/*ion-inline-start:"C:\Users\User\Documents\naichan\src\pages\car-info\car-info.html"*/'<!--\n\n  Generated template for the CarInfoPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n\n\n\n\n    <ion-title>{{carName}}</ion-title>\n\n\n\n    <ion-buttons end>\n\n        <button ion-button clear small icon-start (click)="doAlert()" >\n\n          <ion-icon style="font-size:30px" color="primary" name="checkmark"></ion-icon>\n\n          <span class="right-header-button" color="primary">เสร็จ</span>\n\n        </button>\n\n      </ion-buttons>\n\n\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n    <ion-card class="div-profile-info">\n\n        <div style="height:60px">\n\n        <ion-card-content>\n\n          <ion-card-title>\n\n            {{carName}}\n\n          </ion-card-title>\n\n            <p>\n\n              <span class="profile-tel">{{carName}}</span>\n\n            </p>\n\n          </ion-card-content>\n\n        </div>\n\n        <ion-row no-padding>\n\n          <ion-col text-right>\n\n            <button ion-button clear small icon-start style="color:grey;font-family: \'Kanit\', sans-serif;">\n\n              <ion-icon name=\'brush\'></ion-icon><span style="color:grey;">sdfgdfg</span>\n\n            </button>\n\n          </ion-col>\n\n        </ion-row>\n\n      </ion-card>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\User\Documents\naichan\src\pages\car-info\car-info.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], CarInfoPage);
    return CarInfoPage;
}());

//# sourceMappingURL=car-info.js.map

/***/ })

},[208]);
//# sourceMappingURL=main.js.map